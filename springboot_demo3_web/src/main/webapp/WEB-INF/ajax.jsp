<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>您好Springboot</title>
<script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
     /**
          for语法1:
            for(let i=0;i<xxx.length;i++){
                .....
            }

          for语法2:   index  代表下标
           for(let index in data){
                console.log("测试for循环")
                let user = data[index];
                alert(user.name);
           }

           for语法3:   user当前循环遍历的对象
               for(let user of data){
                    console.log("for循环测试:"+user.name);
               }

           **/


	$(function(){

		$.ajax({
			type : "get",
			url  : "/findAjax",
			//dataType : "text/json/html/xml",		//返回值类型
			async : false,	//关闭异步操作,改为同步的请求
			success : function(data){
				let trs = "";
	            for(let user of data){
	               let id = user.id;
	               let name = user.name;
	               let age = user.age;
	               let sex = user.sex;
	               trs += "<tr align='center'><td>"+id+"</td><td>"+name+"</td><td>"+age+"</td><td>"+sex+"</td></tr>"
	            }
	            $("#tab1").append(trs);
			},
			error : function(){
				alert("请求异常!!!!");
			}
		})



        //1. $.get(url,data,回调函数)
        //findAjax查询后端userList集合信息.
       /*  $.get("/findAjax",function(data){

            $(data).foreach(function(index,user){
				console.log(user);
               })

            let trs = "";
            for(let user of data){
               let id = user.id;
               let name = user.name;
               let age = user.age;
               let sex = user.sex;
               trs += "<tr align='center'><td>"+id+"</td><td>"+name+"</td><td>"+age+"</td><td>"+sex+"</td></tr>"
            }
            $("#tab1").append(trs);
        }); */

	});

</script>





</head>
<body>
	<table id="tab1" border="1px" width="65%" align="center">
		<tr>
			<td colspan="6" align="center"><h3>学生信息</h3></td>
		</tr>
		<tr>
			<th>编号</th>
			<th>姓名</th>
			<th>年龄</th>
			<th>性别</th>
		</tr>
	</table>
</body>
</html>