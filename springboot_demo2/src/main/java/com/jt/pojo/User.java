package com.jt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true) //链式加载
@NoArgsConstructor  //添加无参构造
@AllArgsConstructor //添加全参构造


public class User {
    private Integer id; //pojo必须写包装类型
    private String name; //名称

   /* public User setId(Integer id){
        this.id = id;
        return this;
    }

    public User setName(String name){
        this.name = name;
        return this;
    }*/
}
