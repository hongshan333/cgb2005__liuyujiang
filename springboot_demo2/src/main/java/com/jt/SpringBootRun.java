package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication  //标识我是一个SpringBoot的项目
public class SpringBootRun {

    /**
     * main方法是java程序的唯一入口
     * @param args
     */
    public static void main(String[] args) {
        //加载@SpringBootApplication
        SpringApplication.run(SpringBootRun.class,args);
    }

}
